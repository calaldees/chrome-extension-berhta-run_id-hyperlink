Browser Extension: Bertha `run_id` Hyperlink
============================================

An extension to identify bertha run_id's on DataDog log pages and replace them with hyperlinks

How to use
----------
1. Clone this repo
2. Open Chrome
    * [chrome://extensions](chrome://extensions)
        1. Developer Mode
        2. Load unpacked
            * Select path to cloned repo
3. Visit [https://datadoghq.eu/logs](https://datadoghq.eu/logs)


References
----------

* [Chrome Extension: Get Started](https://developer.chrome.com/extensions/getstarted)
* [Chrome Extension: Content Scripts](https://developer.chrome.com/extensions/content_scripts)
