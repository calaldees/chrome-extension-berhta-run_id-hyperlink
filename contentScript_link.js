
// Utils -----------------------------------------------------------------------

// https://medium.com/@TCAS3/debounce-deep-dive-javascript-es6-e6f8d983b7a1
// https://chrisboakes.com/how-a-javascript-debounce-function-works/
const debounce = (fn, time) => {
    let timeout;
    return function() {
        const functionCall = () => fn.apply(this, arguments);
        clearTimeout(timeout);
        timeout = setTimeout(functionCall, time);
    }
}
const isSetEqual = (a, b) => a.size === b.size && [...a].every(value => b.has(value));


// Constants -------------------------------------------------------------------

const REGEXS = {
    multisample_run_id: {
        regex: new RegExp(`(run_id[:\\s"']+)([0123456789abcdef]{32})`, 'g'),
        replacement: '$1<a href="${url}/tracking/runs?workflow_run_id_hash=$2" target="_blank">$2</a>',
    },
    run_id: {
        regex: new RegExp(`(run_id[:\\s"']+)(\\d{5,8})`, 'g'),
        replacement: '$1<a href="${url}/tracking/runs/$2/status" target="_blank">$2</a>',
    },
}


// DataDog State ---------------------------------------------------------------

function datadog_filters() {
    return new Set([...document.getElementsByClassName('pill_field')].map((element)=>element.textContent.toLowerCase()));
}


// Replace REGEX's for DataDog elements ----------------------------------------

function replace_hyperlinks() {
    // Ascertain environment url for these log messages
    let environment_url = 'unknown';
    if (isSetEqual(datadog_filters(), new Set(['env:prod', 'service:bertha-orch']))) {
        environment_url = 'https://bio-prod-bertha-orch-01.gel.zone';
    }
    // Replace for every log message
    for (let element of [
        ...document.getElementsByClassName('dt-event_message'),
        ...document.getElementsByClassName('ui_typography_code__code'),
    ]) {
        _replace_hyperlink(element, environment_url);
    }
    console.log('run_id links updated', environment_url);
}
function _replace_hyperlink(element, url) {
    for (let {regex, replacement} of Object.values(REGEXS)) {
        element.innerHTML = element.innerHTML.replace(
            regex,
            replacement.replace('${url}', url),
        );
    }
}


// MutationObserver ------------------------------------------------------------
// Listen to specific DOM events
// https://medium.com/allenhwkim/getting-started-mutationobserver-9d891239cf34

/*
const observer_debug = new MutationObserver(debounce((mutationsList) => {
    for(var mutation of mutationsList) {
        console.log(`${mutation.type} has been changed.`);
    }
}), 1000);
*/
const observer = new MutationObserver(debounce((mutationsList) => {
    replace_hyperlinks();
}), 1000);
observer.observe(
    document.getElementsByClassName('drillthrough-frame')[0],
    {childList: true},
);
observer.observe(
    document.getElementsByClassName('layout_side-panel')[0],
    {attributes: true,},
)
